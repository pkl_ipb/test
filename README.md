# test Indogram
*by VhieArch*

---

[October](http://octobercms.com) is a powerful CMS based on [Laravel PHP Framework](http://laravel.com). 

## Prerequisites

1. PHP > 5.5.9
1. MySQL
1. [Composer](http://getcomposer.org)
1. [Bower](http://bower.io)

## Getting Started

1. Clone to your base project directory.

	```
	git clone https://bitbucket.org/pkl_ipb/test.git <project-name>
	```

2. Install composer dependencies.

	```
	composer install
	```

3. Create configuration file `.env` (copy from `.env.example`) and set the database configuration.

	```
	DB_HOST=localhost
	DB_DATABASE=<database-name>
	DB_USERNAME=<database-user>
	DB_PASSWORD=<database-password>
	```

4. Migrate October database.

	```
	php artisan october:up
	```

5. Install frontend library (will create folder `bower_components`.

	```
	bower install
	```

6. For security reason, please generate new application key.

	```
	php artisan key:generate
	```


## Plugins

In this boilerplate, **we've installed**:

1. [RainLab.User](https://octobercms.com/plugin/rainlab-user)
1. [RainLab.Pages](https://octobercms.com/plugin/rainlab-pags)
1. [RainLab.Sitemap](https://octobercms.com/plugin/rainlab-sitemap)
1. [RainLab.GoogleAnalytics](https://octobercms.com/plugin/rainlab-googleanalytics)
1. [October.Drivers](https://octobercms.com/plugin/october-drivers)
1. [Bedard.Debugbar](https://octobercms.com/plugin/bedard-debugbar)

More plugins that we recommend (not installed yet):

1. [RainLab.Blog](https://octobercms.com/plugin/rainlab-blog)
1. [AnandPatel.WysiwygEditors](https://octobercms.com/plugin/anandpatel-wysiwygeditors)
1. [eBussola.Feedback](https://octobercms.com/plugin/ebussola-feedback)
1. [Flynsarmy.SocialLogin](https://)

To install plugin, run the command:

```
php artisan plugin:install <plugin-name>
```

## Frontend Libraries

All frontend libraries are managed using **bower**. These packages are installed by default:

1. [Bootstrap](https://getbootstrap.com)
2. [jQuery](http://jquery.com)
3. [Font Awesome](https://fortawesome.github.io/Font-Awesome)
4. [Parsley JS](http://parsleyjs.org)
5. [Bootstrap Notify](http://bootstrap-notify.remabledesigns.com)
6. [Animate CSS](https://daneden.github.io/animate.css/)
7. [FastClick](https://github.com/ftlabs/fastclick)

To install additional library, run the command:

```
bower install --save <package-name>
```

## Coding Standards

Please follow the following guides and code standards:

* [PSR 4 Coding Standards](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md)
* [PSR 2 Coding Style Guide](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)
* [PSR 1 Coding Standards](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md)