$(document).ready(function() {

    // Attach fastclick
    FastClick.attach(document.body);

    // Activate tooltip bootstrap
    $('[data-toggle=tooltip]').tooltip({
        container: 'body',
    });

    // Add your own custom script
    // ...
    //show-hide
    $('.tab a').on('click', function (e) {

      e.preventDefault();

      $(this).parent().addClass('active');
      $(this).parent().siblings().removeClass('active');

      target = $(this).attr('href');

      $('.tab-content > div').not(target).hide();

      $(target).fadeIn(600);

    });

    //loginModal
    $( ".input" ).focusin(function() {
      $( this ).find( "span" ).animate({"opacity":"0"}, 200);
    });

    $( ".input" ).focusout(function() {
      $( this ).find( "span" ).animate({"opacity":"1"}, 300);
    });

    $(".login").submit(function(){
      $(this).find(".submit i").removeAttr('class').addClass("fa fa-check").css({"color":"#fff"});
      $(".submit").css({"background":"#2ecc71", "border-color":"#2ecc71"});
      $(".feedback").show().animate({"opacity":"1", "bottom":"-80px"}, 400);
      $("input").css({"border-color":"#2ecc71"});
      return false;
    });

    //alert
    $(".btn-alert").on("click", function() {
      $(".alert").removeClass("in").show();
  	  $(".alert").delay(200).addClass("in").fadeOut(5000);
    });

    //text-editor
    

});
