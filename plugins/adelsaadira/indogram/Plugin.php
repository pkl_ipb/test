<?php namespace Adelsaadira\Indogram;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return [
        'Adelsaadira\Indogram\Components\UploadForm' => 'uploadform',
        'Adelsaadira\Indogram\Components\CommentForm' => 'commentform'
      ];
    }

    public function registerSettings()
    {
    }
}
