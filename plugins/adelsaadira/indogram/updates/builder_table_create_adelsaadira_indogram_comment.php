<?php namespace Adelsaadira\Indogram\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAdelsaadiraIndogramComment extends Migration
{
    public function up()
    {
        Schema::create('adelsaadira_indogram_comment', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('users_id');
            $table->integer('posts_id');
            $table->text('comment');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('adelsaadira_indogram_comment');
    }
}
