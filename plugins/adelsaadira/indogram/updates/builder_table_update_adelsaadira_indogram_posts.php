<?php namespace Adelsaadira\Indogram\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAdelsaadiraIndogramPosts extends Migration
{
    public function up()
    {
        Schema::table('adelsaadira_indogram_posts', function($table)
        {
            $table->renameColumn('user_id', 'users_id');
        });
    }
    
    public function down()
    {
        Schema::table('adelsaadira_indogram_posts', function($table)
        {
            $table->renameColumn('users_id', 'user_id');
        });
    }
}
