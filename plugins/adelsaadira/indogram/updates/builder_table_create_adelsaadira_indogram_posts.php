<?php namespace Adelsaadira\Indogram\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAdelsaadiraIndogramPosts extends Migration
{
    public function up()
    {
        Schema::create('adelsaadira_indogram_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->text('caption')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('adelsaadira_indogram_posts');
    }
}
