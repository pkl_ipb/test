<?php namespace AdelsaAdira\Indogram\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use Redirect;
use Adelsaadira\Indogram\Models\Post;
use Flash;
use DateTime;
use Carbon\Carbon;

class UploadForm extends ComponentBase
{


    public function componentDetails()
    {
        return [
            'name'        => 'Form Upload',
            'description' => 'Form for posting'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSave()
    {
      $post = new Post();

      $post->users_id = Input::get('users_id');

      $post->caption = Input::get('caption');

      $post->created_at = Carbon::now();

      $post->save(null, post('_session_key'));

    }
}
