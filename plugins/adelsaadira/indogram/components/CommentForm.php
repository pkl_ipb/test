<?php namespace AdelsaAdira\Indogram\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use Redirect;
use Adelsaadira\Indogram\Models\Comment;
use Flash;
use DateTime;
use Carbon\Carbon;

class CommentForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Form Comment',
            'description' => 'Form for comment'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onComment($value='')
    {
      $comment = new Comment();

      $comment->posts_id = Input::get('posts_id');

      $comment->users_id = Input::get('users_id');

      $comment->comment = Input::get('comment');

      $comment->created_at = Carbon::now();

      $comment->save();
    }
}
