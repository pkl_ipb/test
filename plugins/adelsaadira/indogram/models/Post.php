<?php namespace Adelsaadira\Indogram\Models;

use Model;

/**
 * Model
 */
class Post extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'adelsaadira_indogram_posts';
    public $belongsTo = [
      'users' => 'Rainlab\User\Models\User'
    ];

    public $hasMany = [
      'comments' => 'AdelsaAdira\Indogram\Models\Comment'
    ];

    public $attachOne = [
        'images' => ['System\Models\File']
    ];

}
